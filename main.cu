//
// Created by mikedev on 13/02/19.
//

#include "ImgOp.cuh"
#include "Timer.cuh"
#include "kernels.cuh"
#include <fstream>
#include <iostream>
#include <string>

void append_time(double time, std::string fname) {
  std::ofstream outfile;

  outfile.open(fname, std::ios_base::app);
  outfile << time << std::endl;
  outfile.close();
}

Img read_ppm(const std::string &imgpath);
void save_img(const Img &img);

int main(int argc, char **argv) {
  if (argc < 2 || argc > 4) {
    std::cerr << "Right syntax is ImBlur {imgpath} [dst_folder] [time_file]"
              << std::endl;
    return 1;
  }
  std::string filepath(argv[1]);
  auto img = read_ppm(filepath);
  auto dst = Img(img.width, img.height);
  auto kernel = uniform_kernel();
  if (argc == 4) {
    std::string fname(argv[3]);
    auto elapsed = blur_img(dst, img, kernel);
    append_time(elapsed, fname);
  } else {
    blur_img(dst, img, kernel);
  }

  std::string outpath("blurred_img.ppm");
  if (argc >= 3) {
    std::string outfolder(argv[2]);
    if (outfolder.back() != '/') {
      outfolder += "/";
    }
    outpath = outfolder + outpath;
  }
  dst.save(outpath);
  return 0;
}

Img read_ppm(const std::string &imgpath) {
  std::ifstream imgfile;
  imgfile.open(imgpath, std::ifstream::in);
  std::string format;
  std::string comment;
  std::string size;
  std::string h_str;
  std::getline(imgfile, format);
  // std::getline(imgfile,  comment);
  std::getline(imgfile, size);
  std::getline(imgfile, h_str);
  int w = std::stoi(size.substr(0, size.find(' ')));
  int h = std::stoi(size.substr(size.find(' '), size.length()));
  auto img = Img(w, h);
  char *bytes = new char[3];
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      imgfile.read(bytes, 3);
      img.red[i * w + j] = bytes[0];
      img.green[i * w + j] = bytes[1];
      img.blue[i * w + j] = bytes[2];
    }
  }
  return img;
}
