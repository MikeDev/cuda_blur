//
// Created by mikedev on 13/02/19.
//

#ifndef IMG
#define IMG

#include "Img.cuh"

Img::Img(int width, int height): width(width), height(height) {
    this->red = new u_int8_t [width * height]; // sizeof(u_int8_t) * width * height
    this->green = new u_int8_t [width * height];
    this->blue = new u_int8_t [width * height];
}


void Img::save(std::string path) {
    std::ofstream output;
    output.open(path, std::fstream::out);
    output << "P6" << std::endl << this->width << " " << this->height << std::endl << 255 << std::endl;
    for(int i = 0; i < (this->height * this->width); i++){
        auto p_red = this->red[i];
        auto p_green = this->green[i];
        auto p_blue = this->blue[i];
        output << (char) p_red << (char) p_green << (char) p_blue;
    }
    output.flush();
    output.close();
}

u_int8_t * Img::get_channel(int i){
    if(i == 0){
        return this->red;
    }
    if(i == 1){
        return this->green;
    }
    if(i == 2){
        return this->blue;
    }
    throw "channel index should be between [0,2]";
}

#endif