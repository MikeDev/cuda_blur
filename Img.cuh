//
// Created by mikedev on 13/02/19.
//

#ifndef BLURIMG_IMG_H
#define BLURIMG_IMG_H

#include <fstream>
#include <array>
#include <iostream>
#include <vector>
#include <random>


class Img {
    public:
        int width;
        int height;
        u_int8_t *red;
        u_int8_t *green;
        u_int8_t *blue;
        Img(int width, int height);
        void save(std::string path);
        u_int8_t* get_channel(int i);


};


#endif //BLURIMG_IMG_H
