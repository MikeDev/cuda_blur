//
// Created by mikedev on 13/02/19.
//

#ifndef IMG_OP
#define IMG_OP

#include "ImgOp.cuh"
#include "base.cuh"
#include "../Timer.cuh"
#ifdef CONSTANT_MEM_ENABLE
    #include "convolution/constant_mem.cuh"
#endif

double blur_img(Img &dst, Img &src, float *kernel){
    if(dst.width != src.width || src.height != dst.height){
        throw std::invalid_argument("Input images should have save dimension");
    }
    u_int8_t *cuda_src_channel;
    u_int8_t *cuda_dst_channel;
    float *kernel_cuda;
    dim3 dimBlock = get_block_dim();
    dim3 dimGrid = get_grid_dim(src.width, src.height);
    auto num_threads = dimBlock.x * dimBlock.x;
    #ifdef CONSTANT_MEM_ENABLE
    auto op_result = cudaMemcpyToSymbol(constant_kernel, (const void *) kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), 0, cudaMemcpyHostToDevice);
    #else
    auto op_result = cudaMalloc((void**) &kernel_cuda, KERNEL_SIZE * KERNEL_SIZE * sizeof(float));
    op_result = cudaMemcpy(kernel_cuda, (const void *) kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), cudaMemcpyHostToDevice);
    #endif
    op_result = cudaMalloc((void**) &cuda_src_channel, src.width * src.height * sizeof(u_int8_t));
    op_result = cudaMalloc((void**) &cuda_dst_channel, dst.width * dst.height * sizeof(u_int8_t));
    double total_time = 0.0;
    for(int ch = 0; ch < 3; ch++){
        auto src_channel = src.get_channel(ch);
        auto dest_channel = dst.get_channel(ch);
        cudaMemcpy(cuda_src_channel, src_channel, src.width * src.height * sizeof(u_int8_t), cudaMemcpyHostToDevice);
        auto t = Timer();
        t.start();
        conv_op<<<dimGrid, dimBlock>>>(cuda_src_channel, cuda_dst_channel, kernel_cuda, KERNEL_SIZE, src.width, src.height);
        cudaDeviceSynchronize();
        t.stop();
        total_time += t.getElapsedTimeInMilliSec();
        auto result_copy = cudaMemcpy(dest_channel, cuda_dst_channel, dst.width * dst.height * sizeof(u_int8_t), cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();
    }
    return total_time;
}

#endif //IMG_OP
