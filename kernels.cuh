//
// Created by mikedev on 11/03/19.
//

#ifndef BLURIMG_KERNELS_H
#define BLURIMG_KERNELS_H

#include <array>
#include <cmath>
#include "constants.cuh"

float *uniform_kernel();

float * gaussian_kernel(float sigma);

#endif //BLURIMG_KERNELS_H
