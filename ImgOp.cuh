
#ifndef IMG_OP_H
#define IMG_OP_H

#include <array>
#include <cmath>
#include "Img.cuh"
#include "ImgOp.cuh"
#include <cstdio>
#include <cstdlib>
#include <cuda.h>
#include "constants.cuh"


float blur_img(Img &dst, Img &src, float *kernel);

#endif //IMG_OP