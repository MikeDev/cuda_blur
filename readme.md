# Cuda image processing

This repo contains Cuda implementation of Kernel Image processing

## How to build

    mkdir cmake-build 
    cd cmake-build
    cmake ..
    make

Note: makefile is only for remote running, you need only CMake to build the project

_build_and_run.py_ is a "shortcut" to do any benchmark with one python script

## Folders

- _img_: contains 3 images at different resolutions used for benchmark purpuoses
- _assembled_: 4 different version of convolution on GPU


