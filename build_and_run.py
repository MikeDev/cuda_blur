import os
import sys
from datetime import datetime, timedelta
from functools import reduce
from pathlib import Path
NRUNS = 30

def executables():
    prefix = "ImBlurCuda"
    for suffix in ['Base', 'Tiling', 'BaseConstant', 'TilingConstant']:
        for convsize in ['3x3', '5x5', '7x7']:
            yield prefix + suffix + convsize

IMAGES = ['4k.ppm', 'fullhd.ppm', 'face.ppm']
BASENAMES = [imgname.split(".", 1)[0] for imgname in IMAGES]

def main(check_output=False):
    os.makedirs('cmake-build-debug', exist_ok=True)
    os.chdir('cmake-build-debug')
    os.system('cmake ..')
    os.system('make')
    print('start run:')
    for executable in executables():
        print(f'\tExecutable {executable}')
        for imgname in IMAGES:
            print(f'\t\timg {imgname}:')
            basename = imgname.split(".", 1)[0]
            timefile = f'../time_{executable}_{basename}.txt'
            if Path(timefile).exists():
                with open(timefile) as f:
                    nrows = len(f.read().split('\n'))
                if nrows >= NRUNS:
                    print(timefile, ' exists yet')
                    continue
            if check_output:
                code = os.system(f'./{executable} ../img/{imgname} ../img/ {timefile}')
                if code != 0:
                    print("\t\t\tError ", code)
                    sys.exit(1)
                    return
                os.system(f'mv ../img/img_blurred.ppm ../img/img_blurred_{executable}_{basename}.ppm')
            else:
                for run in range(NRUNS):
                    print(f'\t\t\trun #{run+1}')
                    code = os.system(f'./{executable} ../img/{imgname} ../img/ {timefile}')
                    if code != 0:
                        print("\t\t\tError ", code)
                        sys.exit(1)
                        return
    sys.exit(0)

if __name__ == '__main__':
    main(True)