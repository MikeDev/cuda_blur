//
// Created by mikedev on 11/03/19.
//


#ifndef BLURIMG_KERNELS
#define BLURIMG_KERNELS

#include "kernels.cuh"


float *uniform_kernel() {
    float *result = new float[KERNEL_SIZE * KERNEL_SIZE];
    for(int i = 0; i < KERNEL_SIZE * KERNEL_SIZE; i++){
        result[i] = 1. / (KERNEL_SIZE * KERNEL_SIZE);
    }
    return result;
};


float * gaussian_kernel(float sigma) {
    float *result = new float[KERNEL_SIZE * KERNEL_SIZE];
    for(int i = 0; i < KERNEL_SIZE; i++){
        for(int j = 0; j < KERNEL_SIZE; j++){
            auto kvalue = 1. / (2. * 3.14 * sigma) * exp(- (pow(i,2.) + pow(j,2.)) / (2. * pow(sigma, 2)));
            result[i * KERNEL_SIZE + j] = kvalue;
        }
    }
    return result;
};

#endif