#ifndef CONSTANTS_CUH
#define CONSTANTS_CUH

#ifdef K3B16
const int KERNEL_SIZE = 3;
const int DIM_BLOCK_X = 16;
#endif
#ifdef K5B16
const int KERNEL_SIZE = 5;
const int DIM_BLOCK_X = 16;
#endif
#ifdef K7B16
const int KERNEL_SIZE = 7;
const int DIM_BLOCK_X = 16;
#endif

#endif