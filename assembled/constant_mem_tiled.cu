#include "../Img.cuh"
#include "../ImgOp.cuh"
#include "../Timer.cuh"
#include "../constants.cuh"
#include <cstdio>
// const int TILEDIM_X = 16;
// const int DIM_BLOCK_X = TILEDIM_X - (KERNEL_SIZE / 2) * 2;
// const int TILE_SIZE = TILEDIM_X * TILEDIM_X;


const int TILEDIM_X = DIM_BLOCK_X + (KERNEL_SIZE / 2) * 2;
const int TILE_SIZE = TILEDIM_X * TILEDIM_X;


__constant__ float constant_kernel[KERNEL_SIZE * KERNEL_SIZE];

dim3 get_grid_dim(int width, int height) {
  auto tot_blocks =
      (int)ceil((width * height) / (DIM_BLOCK_X * DIM_BLOCK_X));
  auto x_blocks = (tot_blocks * DIM_BLOCK_X) % width;
  auto y_blocks = (tot_blocks * DIM_BLOCK_X) / width;
  return dim3(x_blocks, y_blocks, 1);
}

dim3 get_block_dim() { return dim3(DIM_BLOCK_X, DIM_BLOCK_X, 1); }

__device__ float weightedsum_shared(u_int8_t *shared, int start_row,
                                    int start_col, int k_size, int tile_width) {
  auto row = start_row;
  auto col = start_col;
  float result = 0;
  for (int i = 0; i < k_size; i++) {
    for (int j = 0; j < k_size; j++) {
      auto value = shared[((row + i) * tile_width) + (col + j)];
      result += ((float)value) * constant_kernel[i * k_size + j];
    }
  }
  return result;
}

__global__ void conv_op(u_int8_t *src_channel, u_int8_t *dst_channel,
                        float *unused_param, int k_size, int img_width,
                        int img_height) {
  __shared__ u_int8_t shared_pixels[TILEDIM_X][TILEDIM_X];
  auto block = blockIdx.y * img_width + blockIdx.x;
  auto ty = threadIdx.y;
  auto tx = threadIdx.x;
  auto tile_x = tx + k_size / 2;
  auto tile_y = ty + k_size / 2;
  auto i_pixel = block * DIM_BLOCK_X + ty * img_width + tx;
  auto dst_x_pixel = i_pixel % img_width;
  auto dst_y_pixel = i_pixel / img_width;
  if (dst_x_pixel < k_size / 2 || (dst_x_pixel + k_size / 2) >= img_width ||
      dst_y_pixel < k_size / 2 || (dst_y_pixel + k_size / 2) >= img_height) {
    dst_channel[i_pixel] = src_channel[i_pixel];
    return;
  } else {

    shared_pixels[tile_y][tile_x] = src_channel[i_pixel];
    auto i = k_size / 2;
    if (ty <= k_size / 2) {
        shared_pixels[tile_y - i][tile_x] = src_channel[i_pixel - (i * img_width)]; // up
    }
    if (tx <= k_size / 2) {
        shared_pixels[tile_y][tile_x - i] = src_channel[i_pixel - i]; // left
    }
    if (ty >= (blockDim.x - 1 - k_size / 2)) {
        shared_pixels[tile_y + i][tile_x] = src_channel[i_pixel + (i * img_width)]; //down
    }
    if (tx >= (blockDim.x - 1 - k_size / 2)) {
        shared_pixels[tile_y][tile_x + i] = src_channel[i_pixel + i]; //right
    }

    if (tx <= (k_size / 2) && ty <= (k_size / 2)) {
      shared_pixels[tile_y - i][tile_x - i] =
            src_channel[i_pixel - (1 + img_width) * i]; // up left
    } else if (tx >= (blockDim.x - 1 - k_size / 2) && ty <= k_size / 2) {
        shared_pixels[tile_y - i][tile_x + i] =
            src_channel[i_pixel + (1 - img_width) * i]; // up right
    } else if (tx <= k_size / 2 && ty >= (blockDim.x - 1 - k_size / 2)) {
        shared_pixels[tile_y + i][tile_x - i] =
            src_channel[i_pixel + (img_width - 1) * i]; // down left
    } else if (tx >= (blockDim.x - 1 - k_size / 2) && ty >= (blockDim.x - 1 - k_size / 2)) {
        shared_pixels[tile_y + i][tile_x + i] =
            src_channel[i_pixel + (img_width + 1) * i]; // down right
    }

    __syncthreads();
    float result = 0;
    for (int i = 0; i < k_size; i++) {
      for (int j = 0; j < k_size; j++) {
        auto value = shared_pixels[ty+i][tx+j];
        result += value * constant_kernel[i * k_size + j];
      }
    }
    dst_channel[i_pixel] = (u_int8_t) result;
  }
}

float blur_img(Img &dst, Img &src, float *kernel) {
  if (dst.width != src.width || src.height != dst.height) {
    throw std::invalid_argument("Input images should have save dimension");
  }
  u_int8_t *cuda_src_channel;
  u_int8_t *cuda_dst_channel;
  float *kernel_cuda;
  dim3 dimBlock = get_block_dim();
  dim3 dimGrid = get_grid_dim(src.width, src.height);
  auto num_threads = dimBlock.x * dimBlock.x;
  auto op_result = cudaMemcpyToSymbol(
      constant_kernel, (const void *)kernel,
      KERNEL_SIZE * KERNEL_SIZE * sizeof(float), 0, cudaMemcpyHostToDevice);
  op_result = cudaMalloc((void **)&cuda_src_channel,
                         src.width * src.height * sizeof(u_int8_t));
  op_result = cudaMalloc((void **)&cuda_dst_channel,
                         dst.width * dst.height * sizeof(u_int8_t));
  float total_time = 0.0;
  for (int ch = 0; ch < 3; ch++) {
    auto src_channel = src.get_channel(ch);
    auto dest_channel = dst.get_channel(ch);
    cudaMemcpy(cuda_src_channel, src_channel,
               src.width * src.height * sizeof(u_int8_t),
               cudaMemcpyHostToDevice);
    auto t = Timer();
    t.start();
    conv_op<<<dimGrid, dimBlock>>>(cuda_src_channel, cuda_dst_channel,
                                   kernel_cuda, KERNEL_SIZE, src.width,
                                   src.height);
    cudaDeviceSynchronize();
    t.stop();
    total_time += t.getElapsedTimeInMilliSec();
    auto result_copy = cudaMemcpy(dest_channel, cuda_dst_channel,
                                  dst.width * dst.height * sizeof(u_int8_t),
                                  cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
  }
  return total_time;
}
