#include "../Img.cuh"
#include "../Timer.cuh"
#include "../ImgOp.cuh"
#include "../constants.cuh"

dim3 get_grid_dim(int width, int height){
    auto tot_blocks = (int) ceil((width * height) / (DIM_BLOCK_X * DIM_BLOCK_X));
    auto x_blocks = (tot_blocks * DIM_BLOCK_X) % width;
    auto y_blocks = (tot_blocks * DIM_BLOCK_X) / width;
    return dim3(x_blocks, y_blocks, 1);
}

dim3 get_block_dim(){
    return dim3(DIM_BLOCK_X, DIM_BLOCK_X, 1);
}

__device__
float weightedsum(u_int8_t *channel, int start_row, int start_col, int img_width, int size, float *kernel) {
    auto row = start_row;
    auto col = start_col;
    float result = 0;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            auto value = channel[((row + i) * img_width) + (col + j)];
            result += ((float) value) * kernel[i * size + j];
        }
    }
    return result;
}

__global__
void conv_op(u_int8_t * src_channel, u_int8_t* dst_channel, float *kernel, int k_size, int img_width, int img_height){
    auto block = blockIdx.y * img_width + blockIdx.x;
    auto i_pixel = block * blockDim.x + threadIdx.y * img_width + threadIdx.x;
    auto dst_x_pixel = i_pixel % img_width;
    auto dst_y_pixel = i_pixel / img_width;
    if((dst_x_pixel <= (k_size / 2)) || (dst_y_pixel <= (k_size / 2)) || ((dst_x_pixel + (k_size / 2)) >= img_width) || ((dst_y_pixel + (k_size / 2)) >= img_height)){
        dst_channel[dst_y_pixel * img_width + dst_x_pixel] = src_channel[dst_y_pixel * img_width + dst_x_pixel];
    } else {
        auto src_x_pixel = dst_x_pixel - k_size / 2;
        auto src_y_pixel = dst_y_pixel - k_size / 2;
        auto newvalue = (u_int8_t) weightedsum(src_channel, src_y_pixel, src_x_pixel, img_width, k_size, kernel);
        dst_channel[dst_y_pixel * img_width + dst_x_pixel] = newvalue;
    }
}

float blur_img(Img &dst, Img &src, float *kernel){
    if(dst.width != src.width || src.height != dst.height){
        throw std::invalid_argument("Input images should have save dimension");
    }
    u_int8_t *cuda_src_channel;
    u_int8_t *cuda_dst_channel;
    float *kernel_cuda;
    dim3 dimBlock = get_block_dim();
    dim3 dimGrid = get_grid_dim(src.width, src.height);
    auto num_threads = dimBlock.x * dimBlock.x;
    auto op_result = cudaMalloc((void**) &kernel_cuda, KERNEL_SIZE * KERNEL_SIZE * sizeof(float));
    op_result = cudaMemcpy(kernel_cuda, (const void *) kernel, KERNEL_SIZE * KERNEL_SIZE * sizeof(float), cudaMemcpyHostToDevice);
    op_result = cudaMalloc((void**) &cuda_src_channel, src.width * src.height * sizeof(u_int8_t));
    op_result = cudaMalloc((void**) &cuda_dst_channel, dst.width * dst.height * sizeof(u_int8_t));
    float total_time = 0.0;
    for(int ch = 0; ch < 3; ch++){
        auto src_channel = src.get_channel(ch);
        auto dest_channel = dst.get_channel(ch);
        cudaMemcpy(cuda_src_channel, src_channel, src.width * src.height * sizeof(u_int8_t), cudaMemcpyHostToDevice);
        auto t = Timer();
        t.start();
        conv_op<<<dimGrid, dimBlock>>>(cuda_src_channel, cuda_dst_channel, kernel_cuda, KERNEL_SIZE, src.width, src.height);
        cudaDeviceSynchronize();
        t.stop();
        total_time += t.getElapsedTimeInMilliSec();
        auto result_copy = cudaMemcpy(dest_channel, cuda_dst_channel, dst.width * dst.height * sizeof(u_int8_t), cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();
    }
    return total_time;
}
